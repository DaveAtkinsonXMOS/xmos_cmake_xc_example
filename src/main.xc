#include <xhdr.h>

#include <stdio.h>

int main(void)
{
    par {
        {
            printf("Hello %s world\n", XMOS_STRING);
        }
        {
            int x = 0;

            for(;;) {
                x = x + 1;
            }
        }
    }

    return 0;
}
